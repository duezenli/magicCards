#include <iostream>
#include "functions.h"
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <vector>
#include "levenD.h"

// Aufgabe 3
//Given a file of cards, return a list consisting of card objects
std::list<Cards> getListOfCards(const std::string &filename) {
    std::ifstream file(filename);
    if (!file) {
        throw std::runtime_error("Error: Unable to open file " + filename);
    }
    //temp string to save line
    std::string temp;
    //Output
    std::list<Cards> CardsList;

    //get every line of scrambled.txt
    while (std::getline(file, temp)) {
        std::stringstream streamData(temp);
        //"cut" the string at the vertical bars(|)
        char delim = '|';
        //string vector to hold the cut parts
        std::vector<std::string> stringVector;
        //temp to store Card attributes
        std::string attr;

        //go over every line, cut line accordingly and push it at the back of the vector
        while (getline(streamData, attr, delim)) {
            stringVector.push_back(attr);
        }
        //assign every cut out part to the according attribute
        std::string name = stringVector[0];
        std::string mana = stringVector[1];
        std::string cmc = stringVector[2];
        std::string type = stringVector[3];
        std::string count = stringVector[4];

        //create card object and assign attributes to it
        Cards card;
        card.cmc = cmc;
        card.mana = mana;
        card.name = name;
        card.type = type;
        card.count = count;

        //push Card object at the end of the List
        CardsList.push_back(card);
    }

    //close .txt file
    file.close();


    //return the cards as a list
    return CardsList;
}


// Aufgabe 4
//Given a list of cards, save the card objects back into a file
void saveCardsToFile(const std::list<Cards> &cardsList, const std::string &filename) {
    //Create new file
    std::ofstream file(filename);

    //Check if file can be opened
    if (!file) {
        throw std::runtime_error("Error: Unable to open file.");
    }
    //Print every card object in CardsList into separate Line, add the vertical bar again
    for (const auto &card: cardsList) {
        file << card.name << "|" << card.mana << "|" << card.cmc << "|" << card.type << "|" << card.count << std::endl;
    }

    //Close file
    file.close();
}

//Aufgabe 5
std::list<std::string> referenceNames(const std::string &referenceFile) {
    std::ifstream file(referenceFile);
    //check if file can be opened
    if (!file) {
        throw std::runtime_error("Error: Unable to open file " + referenceFile);
    }
    //output
    std::list<std::string> namesList;
    //temp for names
    std::string name;
    //read every line
    while (std::getline(file, name)) {
        namesList.push_back(name);
    }

    //close file
    file.close();

    //return list
    return namesList;
}


// Aufgabe 6
//Calculate the levenshtein distance for two lists, replace the broken names with the intact names if possible and save
// in a new .txt file
void levenshteinDistance(const std::list<std::string> &intactNames, std::list<Cards> &brokenNames) {
    for (auto &card: brokenNames) {
        //reference value
        double referenceV = 0.2675 * card.name.length();
        /* On macOS use this loop instead
        for (const auto &intactName: intactNames) {

            //Following two lines are to remove the /r at the end of the string
            std::string newIntact = intactName;
            newIntact.erase(newIntact.size() - 1);
            //calculate levenD of card for every intactName
            int levenDist = levenD(card.name, newIntact);

            //if distance smaller than 26.75% of the card name length...
            if (levenDist < referenceV) {
                //replace broken name with intact name
                card.name = newIntact;
                //break and go on to next card
                break;
            }
        } */
        for (const auto &intactName: intactNames) {
            //calculate levenD of card for every intactName
            int levenDist = levenD(card.name, intactName);

            //if distance smaller than 26.75% of the card name length...
            if (levenDist < referenceV) {
                //replace broken name with intact name
                card.name = intactName;
                //break and go on to next card
                break;
            }
        }
    }
    //Aufgabe 7
    saveCardsToFile(brokenNames, "repaired.txt");
}




