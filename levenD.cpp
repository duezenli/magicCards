#include "levenD.h"

//Helper function, calculate minimum of three values
int min(int x, int y, int z) {
    if (x < y && x < z) {
        return x;
    } else if (y < z) {
        return y;
    } else {
        return z;
    }
}

//Given two strings,calculate the levenshtein distance between them and return it as an integer
int levenD(const std::string &s, const std::string &t) {
    //Length of first string
    int n = s.length();
    //Length of second string
    int m = t.length();
    int cost = 0;

    //Matrix to hold the levenshtein distance (longest string is 174)
    int Matrix[175][175];
    // Initialize the first row and column of the matrix,[0][0] is empty
    for (int i = 0; i <= n; i++) {
        Matrix[i][0] = i;
    }
    for (int j = 0; j <= m; j++) {
        Matrix[0][j] = j;
    }

    // Calculate the Levenshtein distances for all pairs of characters
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {

            if (s[i - 1] == t[j - 1])
                cost = 0;
            else
                cost = 1;


            //M[i][j] := min(rep, ins, del)
            int replace = Matrix[i - 1][j - 1] + cost;
            int insert = Matrix[i][j - 1] + 1;
            int del = Matrix[i - 1][j] + 1;
            Matrix[i][j] = min(replace, insert, del);

        }
    }
    //matrix entry at the bottom right which represents the levenshteinDistance
    int LevenDist = Matrix[n][m];

    //return the distance
    return LevenDist;
}

